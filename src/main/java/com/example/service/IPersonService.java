
package com.example.service;

import com.example.dto.GroupDTO;
import com.example.dto.PersonDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IPersonService {
    void addPerson(PersonDTO personDTO);
    void removePerson(String id);
    void updatePerson(String id,PersonDTO personDTO);
    PersonDTO getPerson(String id);
    List<PersonDTO> getPersons();
    List<GroupDTO> groupsPersonBelongTo (String personId);
}