package com.example.service;

import com.example.dto.GroupDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IGroupService  {
    GroupDTO groupName(String groupId);
    void createGroup(GroupDTO groupDTO);
    void changeGroupName(String groupId,GroupDTO groupDTO);
    void removeGroup(String groupId);
    List<GroupDTO> getGroups();
}