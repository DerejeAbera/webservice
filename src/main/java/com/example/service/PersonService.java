package com.example.service;

import com.example.dto.GroupDTO;
import com.example.dto.PersonDTO;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PersonService implements IPersonService {

    ArrayList<PersonDTO> personDTOList=new ArrayList<>();

    @Override
    public List<GroupDTO> groupsPersonBelongTo(String personId) {
        List<GroupDTO> group1 = new ArrayList<>();
        // Hämtas från Webtjänst 2
        Flux<GroupDTO> response = WebClient.create("http://localhost:8080/groups")
                .get()
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToFlux(GroupDTO.class);

        List<GroupDTO> groups = response.collect(Collectors.toList()).share().block();


        for (GroupDTO group : groups) {
            if (group.getMemberIds().contains(personId)) {
                group1.add(group);
            }

        }

        return group1;
    }

    @Override
    public void addPerson(PersonDTO personDTO) {
        personDTOList.add(personDTO);

    }

    @Override
    public void removePerson(String id) {
        for (int i = 0; i < personDTOList.size(); i++) {
            if (personDTOList.get(i).getId().equals(id)) {
                personDTOList.remove(personDTOList.get(i));
            }
        }
    }

    @Override
    public void updatePerson(String id, PersonDTO personDTO) {
        for (int i = 0; i < personDTOList.size(); i++) {
            if (personDTOList.get(i).getId().equals(id)) {
                personDTOList.get(i).setFirstName(personDTOList.get(i).getFirstName());
                personDTOList.get(i).setLastName(personDTOList.get(i).getLastName());
            }
        }
    }

    @Override
    public PersonDTO getPerson(String id) {
        for (int i = 0; i < personDTOList.size(); i++) {
            if (personDTOList.get(i).getId().equals(id)) {
                return personDTOList.get(i);
            }
        }
        return null;
    }

    @Override
    public List<PersonDTO> getPersons() {
        if (personDTOList != null)
            return personDTOList;
        return null;
    }

}