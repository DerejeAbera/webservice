package com.example.service;

import com.example.dto.GroupDTO;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;

@Component
public class GroupService implements IGroupService {

    ArrayList<GroupDTO> groupDTOArrayList=new ArrayList<>();

    @Override
    public GroupDTO groupName(String groupId) {
        for (int i = 0; i < groupDTOArrayList.size(); i++) {
            if (groupDTOArrayList.get(i).getGroupId().equals(groupId)) {
                return groupDTOArrayList.get(i);
            }
        }
        return null;
    }

    @Override
    public void createGroup(GroupDTO groupDTO) {
        groupDTOArrayList.add(groupDTO);
    }

    @Override
    public void changeGroupName(String groupId,GroupDTO groupDTO) {

        for (int i = 0; i < groupDTOArrayList.size(); i++) {
            if (groupDTOArrayList.get(i).getGroupId().equals(groupId)) {
                groupDTOArrayList.get(i).setGroupName(groupDTOArrayList.get(i).getGroupName());
                //groupDTOArrayList.get(i).setLastName(groupDTOArrayList.get(i).getLastName());
            }
        }
    }

    @Override
    public void removeGroup(String groupId) {
        for (int i = 0; i < groupDTOArrayList.size(); i++) {
            if (groupDTOArrayList.get(i).getGroupName().equals(groupId)) {
                groupDTOArrayList.remove(groupDTOArrayList.get(i));
            }
        }
    }

    @Override
    public List<GroupDTO> getGroups() {
        if (groupDTOArrayList != null)
            return groupDTOArrayList;
        return null;
    }
}