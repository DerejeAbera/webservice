package com.example.controller;

import com.example.dto.GroupDTO;
import com.example.dto.PersonDTO;
import com.example.service.PersonService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value="/persons")
//@Stateless
public class PersonController {

    private final PersonService personService;


    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/{id}/groups")
    ResponseEntity<List<GroupDTO>> groupsPersonBelongTo (@PathVariable String id){
        try {
            List<GroupDTO> personGroups=personService.groupsPersonBelongTo(id);
            return ResponseEntity.ok(personGroups);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    void addPerson(@RequestBody PersonDTO personDTO){
        try {
            // personDTO.setGroups();
            personDTO.setId(UUID.randomUUID().toString());
            personService.addPerson(personDTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @GetMapping("/{id}")
    ResponseEntity<PersonDTO> getPerson(@PathVariable String id){
        PersonDTO personDTO= personService.getPerson(id);
        return ResponseEntity.ok(personDTO);
    }

    @GetMapping
    ResponseEntity<List<PersonDTO>> getAllPersons(){
        List<PersonDTO> personDTOList = personService.getPersons();
        return ResponseEntity.ok(personDTOList);
    }

    @DeleteMapping("/{id}")
    ResponseEntity<PersonDTO> removePerson(@PathVariable String id){
        try {
            personService.removePerson(id);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
    @PutMapping("/{id}")
    ResponseEntity<PersonDTO> updatePerson(@PathVariable String id,@RequestBody PersonDTO personDTO){
        try {
            if(id!=null&&personDTO!=null) {
                personService.updatePerson(id, personDTO);
                return ResponseEntity.status(HttpStatus.OK).build();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
}