package com.example.controller;

import com.example.dto.GroupDTO;
import com.example.service.GroupService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value="/groups")
public class GroupController {
    private final GroupService groupService ;

    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    void addGroup(@RequestBody GroupDTO groupDTO){
        try {
            groupDTO.setGroupId(UUID.randomUUID().toString());
            groupService.createGroup(groupDTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // creatGroup
    @GetMapping("/{id}")
    ResponseEntity<GroupDTO> groupName(@PathVariable String id){
        GroupDTO groupDTO= groupService. groupName(id);
        return ResponseEntity.ok(groupDTO);
    }
    @GetMapping
    ResponseEntity<List<GroupDTO>> getGroups(){
        List<GroupDTO> groupDTOList = groupService.getGroups();
        return ResponseEntity.ok(groupDTOList);
    }


    @DeleteMapping("/{id}")
    ResponseEntity<GroupDTO> removeGroup(@PathVariable String id){
        try {
            groupService.removeGroup(id);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
    @PutMapping("/{id}")
    ResponseEntity<GroupDTO> changeGroupName(@PathVariable String id,@RequestBody GroupDTO groupDTO){
        try {
            if(id!=null&&groupDTO!=null) {
                groupService.changeGroupName(id, groupDTO);
                return ResponseEntity.status(HttpStatus.OK).build();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
}